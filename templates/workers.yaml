{{- $envAll := . }}
{{- range $machine_deployment_name, $machine_deployment_specs := .Values.machine_deployments }}

  {{/*********** Finalize the definition of the machine_deployments.X
  item by merging the machine_deployment_default */}}
  {{- $machine_deployment_def := dict -}}
  {{- $machine_deployment_def := deepCopy ($envAll.Values.machine_deployment_default | default dict) -}}
  {{- $machine_deployment_def := mergeOverwrite $machine_deployment_def $machine_deployment_specs -}}

  {{/*********** Prepare complete labels used in generated object */}}
  {{- $labels := dict -}}
  {{- $labels := deepCopy ($machine_deployment_def | dig "metadata" "labels" dict) -}}
  {{- $_ := mergeOverwrite $labels (include "sylva-capi-cluster.labels" $envAll | fromYaml) -}}

  {{/*********** Set the default machine_deployments.X.infra_provider, if not specifically defined */}}
  {{ if not ($machine_deployment_def.infra_provider) }}
  {{- $_ := set $machine_deployment_def "infra_provider" $envAll.Values.capi_providers.infra_provider -}}
  {{ end }}

  {{/*********** Set the default machine_deployments.X.capo.identity_ref_secret.name, if not specifically defined for CAPO MD
  and create the Secret containing the particular OpenStack cloudrc file data for this MD, if defined */}}
  {{ if eq $machine_deployment_def.infra_provider "capo" }}
    {{ if not $machine_deployment_def.capo.identity_ref_secret }}
    {{- $_ := mergeOverwrite $machine_deployment_def.capo (dict "identity_ref_secret" (dict "name" (printf "%s-capo-cloud-config" $envAll.Values.name))) -}}
    {{ else if $machine_deployment_def.capo.identity_ref_secret.clouds_yaml }}
      {{ if not $machine_deployment_def.capo.identity_ref_secret.name }}
      {{- $_ := mergeOverwrite $machine_deployment_def.capo.identity_ref_secret (dict "name" (printf "%s-capo-cloud-config" (printf "%s-%s" $envAll.Values.name $machine_deployment_name))) -}}
      {{ end }}
    {{- tuple $envAll $machine_deployment_def.capo.identity_ref_secret.name $machine_deployment_def.capo.identity_ref_secret.clouds_yaml | include "OpenStackIdentityReferenceSecret" }}
    {{ end }}
  {{ end }}

  {{/*********** Set the default machine_deployments.X.kubelet_extra_args to empty dict, if not specifically defined */}}
  {{ if not ($machine_deployment_def.kubelet_extra_args) }}
  {{- $_ := set $machine_deployment_def "kubelet_extra_args" (dict) -}}
  {{ end }}

  {{/*********** Set the default machine_deployments.X.additional_commands to empty dict, if not specifically defined */}}
  {{ if not ($machine_deployment_def.additional_commands) }}
  {{- $_ := set $machine_deployment_def "additional_commands" (dict) -}}
  {{ end }}

  {{/*********** Set the default machine_deployments.X.kubeadm to empty dict, if not specifically defined */}}
  {{ if not ($machine_deployment_def.kubeadm) }}
  {{- $_ := set $machine_deployment_def "kubeadm" (dict) -}}    {{/* FIXME  ------------------------------------------------------------------------ */}}
  {{ end }}


  {{- $config_template_resource_kind := "" }}
  {{- $config_template_helm_include := "" }}
  {{- if eq $envAll.Values.capi_providers.bootstrap_provider "cabpr" -}}
    {{ $config_template_resource_kind = "RKE2ConfigTemplate" -}}
    {{ $config_template_helm_include = "RKE2ConfigTemplateSpec" -}}
  {{- else if eq $envAll.Values.capi_providers.bootstrap_provider "cabpk" -}}
    {{ $config_template_resource_kind = "KubeadmConfigTemplate" -}}
    {{ $config_template_helm_include = "KubeadmConfigTemplateSpec" -}}
  {{- end -}}

  {{- $config_template_resource_template_spec := tuple $envAll $machine_deployment_def | include $config_template_helm_include }}
  {{- $config_template_resource_name := printf "%s-%s-%s" $envAll.Values.name $machine_deployment_name ($config_template_resource_template_spec | sha1sum | trunc 10) }}

---
apiVersion: {{ $envAll.Values.apiVersions.MachineDeployment }}
kind: MachineDeployment
metadata:
  name: {{ $envAll.Values.name }}-{{ $machine_deployment_name }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    cluster.x-k8s.io/cluster-name: {{ $envAll.Values.name }}
{{ $labels | toYaml | indent 4 }}
  annotations: {{ $machine_deployment_def | dig "metadata" "annotations" dict | toYaml | nindent 4 }}
  {{ $baseSpec :=  tuple $envAll $machine_deployment_def $config_template_resource_kind $config_template_resource_name $machine_deployment_name | include "MachineDeploymentSpec" | indent 0 | fromYaml }}
  {{ $finalSpecs := mergeOverwrite $baseSpec ($machine_deployment_def.machine_deployment_spec | default dict) }}
spec: {{ $finalSpecs | toYaml | nindent 2 }}


{{/*********** Implement mixed CP & MD infra providers trick, if the two differ, by creating
another XCluster of the $machine_deployment_def.infra_provider type (DockerCluster/OpenStackCluster/VSphereCluster/Metal3Cluster)
on top of the XCluster of the $envAll.Values.capi_providers.infra_provider type, created in templates/cluster.yaml,
both owned by one common Cluster.cluster.x-k8s.io resource */}}
{{ if not (eq $machine_deployment_def.infra_provider $envAll.Values.capi_providers.infra_provider) }}
---
{{ if eq $machine_deployment_def.infra_provider "capd" }}
apiVersion: {{ $envAll.Values.apiVersions.DockerCluster }}
kind: DockerCluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  controlPlaneEndpoint:
    host: {{ $envAll.Values.cluster_external_ip }}
    port: 6443
{{ else if eq $machine_deployment_def.infra_provider "capo" }}
apiVersion: {{ $envAll.Values.apiVersions.OpenStackCluster }}
kind: OpenStackCluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  cloudName: capo_cloud
  externalNetworkId: {{ $envAll.Values.capo.network_id }}
  network:
    id: {{ $envAll.Values.capo.network_id }}
  apiServerFixedIP: {{ eq $envAll.Values.cluster_public_ip "" | ternary $envAll.Values.cluster_external_ip $envAll.Values.cluster_public_ip }}
  identityRef:
    kind: Secret
    name: {{ $machine_deployment_def.capo.identity_ref_secret }}
  managedSecurityGroups: true
  disableAPIServerFloatingIP: true
  allowAllInClusterTraffic: true
  tags:
  - {{ $envAll.Values.capo.resources_tag }}
{{ if $envAll.Values.capo.control_plane_az }}
  controlPlaneAvailabilityZones: {{ $envAll.Values.capo.control_plane_az | toJson }}
{{ end }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
apiVersion: {{ $envAll.Values.apiVersions.VSphereCluster }}
kind: VSphereCluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  controlPlaneEndpoint:
    host: {{ $envAll.Values.cluster_external_ip }}
    port: 6443
  identityRef:
    kind: Secret
    name: {{ $envAll.Values.name }}
  server: {{ $envAll.Values.capv.server }}
  thumbprint: {{ $envAll.Values.capv.tlsThumbprint }}
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
apiVersion: {{ $envAll.Values.apiVersions.Metal3Cluster }}
kind: Metal3Cluster
metadata:
  name: {{ $envAll.Values.name }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
  controlPlaneEndpoint:
    host: {{ $envAll.Values.cluster_external_ip }}
    port: 6443
  noCloudProvider: true
{{ end }}
{{ end }}
---
{{ tuple $envAll $machine_deployment_name $machine_deployment_def $labels | include "XMachineTemplate-MD" }}
---
apiVersion: {{ index $envAll.Values.apiVersions $config_template_resource_kind }}
kind: {{ $config_template_resource_kind }}
metadata:
  name: {{ $config_template_resource_name }}
  namespace: {{ $envAll.Release.Namespace }}
  labels: {{ $labels | toYaml | nindent 4 }}
spec:
  template:
    spec: {{- $config_template_resource_template_spec | nindent 6 }}
{{- end }}
