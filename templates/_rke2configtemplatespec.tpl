{{- define "RKE2ConfigTemplateSpec" }}
  {{- $envAll := index . 0 -}}
  {{- $machine_deployment_def := index . 1 -}}

  {{- $machine_kubelet_extra_args := $machine_deployment_def.kubelet_extra_args -}}
  {{- $machine_rke2_specs := $machine_deployment_def.rke2 -}}
  {{- $machine_additional_commands := $machine_deployment_def.additional_commands -}}
  {{- $machine_additional_files := $machine_deployment_def.additional_files -}}

  {{/*********** Initialize the components of the RKE2ConfigTemplate.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_kubelet_extra_args $machine_rke2_specs $machine_additional_files | include "base-RKE2ConfigTemplateSpec" | fromYaml }}
  {{- $infra := include (printf "%s-RKE2ConfigTemplateSpec" $envAll.Values.capi_providers.infra_provider) $envAll | fromYaml }}

agentConfig: {{ mergeOverwrite $base.agentConfig $infra.agentConfig (dict "kubelet" (dict "extraArgs" (concat $base.agentConfig.kubelet.extraArgs $infra.agentConfig.kubelet.extraArgs))) | toYaml | nindent 2 }}
{{- $md_additional_commands := deepCopy ($envAll.Values.additional_commands | default dict) -}}
{{- if $machine_additional_commands }}
  {{- tuple $md_additional_commands $machine_additional_commands | include "merge-append" }}
{{- end }}
preRKE2Commands:
  {{ $infra.preRKE2Commands | toYaml | nindent 2 }}
  {{ $base.preRKE2Commands | toYaml | nindent 2 }}
{{- if $md_additional_commands.pre_bootstrap_commands }}
  {{ $md_additional_commands.pre_bootstrap_commands | toYaml | nindent 2 }}
{{- end }}
files: {{ concat $base.files $infra.files | toYaml | nindent 2 }}
{{ $md_rke2_nodeAnnotations := mergeOverwrite (deepCopy $envAll.Values.rke2.nodeAnnotations) ($machine_rke2_specs.nodeAnnotations | default dict) }}
postRKE2Commands:
{{- if or $md_additional_commands.post_bootstrap_commands $md_rke2_nodeAnnotations }}
  {{- if $md_additional_commands.post_bootstrap_commands }}
  {{ $md_additional_commands.post_bootstrap_commands | toYaml | nindent 2 }}
  {{- end }}
  {{/* workaround for the fact that CABPR v0.2.0 RKE2ConfigTemplate.spec.template.spec.agentConfig.nodeAnnotations does not result in node annotations; see https://gitlab.com/sylva-projects/sylva-core/-/issues/417#note_1668330146 */}}
  {{- if $md_rke2_nodeAnnotations -}}
  {{ range $node_annotation_key, $node_annotation_value := $md_rke2_nodeAnnotations }}
  - /var/lib/rancher/rke2/bin/kubectl --kubeconfig /var/lib/rancher/rke2/agent/kubelet.kubeconfig annotate node $(hostname) {{ printf "%s=%s" $node_annotation_key $node_annotation_value }}
  {{- end -}}
  {{- end }}
{{- else }}
  []
{{- end }}
{{- end }}
