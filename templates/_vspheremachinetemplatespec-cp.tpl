{{ define "VSphereMachineTemplateSpec-CP" }}
cloneMode: {{ .Values.capv.template_clone_mode }}
datacenter: {{ .Values.capv.dataCenter | quote }}
{{- if .Values.capv.dataStore }}
datastore: {{ .Values.capv.dataStore }}
{{- end }}
diskGiB: {{ pluck "diskGiB" .Values.capv (.Values.control_plane.capv | default dict) | last | required "'diskGiB' needs to be defined under capv or control_plane.capv" }}
folder: {{ .Values.capv.folder }}
memoryMiB: {{ pluck "memoryMiB" .Values.capv (.Values.control_plane.capv | default dict) | last | required "'memoryMiB' needs to be defined under capv or control_plane.capv" }}
network:
  devices:
  {{- range $network_key, $network_def := .Values.capv.networks }}
    - dhcp4: {{ $network_def.dhcp4 | default false }}
      networkName: {{ $network_def.networkName }}
  {{- end }}
numCPUs: {{ pluck "numCPUs" .Values.capv (.Values.control_plane.capv | default dict) | last | required "'numCPUs' needs to be defined under capv or control_plane.capv" }}
resourcePool: {{ .Values.capv.resourcePool }}
server: {{ .Values.capv.server }}
{{- if .Values.capv.storagePolicyName }}
storagePolicyName: {{ .Values.capv.storagePolicyName }}
{{- end }}
template: {{ pluck "image_name" .Values.capv .Values.control_plane.capv | last | required "'image_name' needs to be defined under capv or control_plane.capv" }}
thumbprint: {{ .Values.capv.tlsThumbprint }}
{{ end }}
