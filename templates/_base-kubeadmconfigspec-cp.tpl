{{- define "base-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    taints: []
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy .Values.kubelet_extra_args) (deepCopy .Values.control_plane.kubelet_extra_args) | toYaml | nindent 6 }}
joinConfiguration:
  nodeRegistration:
    taints: []
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy .Values.kubelet_extra_args) (deepCopy .Values.control_plane.kubelet_extra_args) | toYaml | nindent 6 }}
clusterConfiguration: {}
ntp: {{ .Values.ntp | toYaml | nindent 2 }}
preKubeadmCommands:
  - set -e
  {{- if and (not (eq .Values.capi_providers.infra_provider "capd")) .Values.dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if .Values.proxies.http_proxy }}
  - systemctl daemon-reload
  - systemctl restart containerd.service
  {{- end }}
  - echo "Preparing Kubeadm bootstrap" > /var/log/my-custom-file.log
files:
{{ $kubeadmcpfiles := list }}
{{- if or (eq .Values.capi_providers.infra_provider "capo") (eq .Values.capi_providers.infra_provider "capv") (eq .Values.capi_providers.infra_provider "capm3") }}
    {{- $kubeadmcpfiles = include "kubernetes_kubeadm_vip" . | append $kubeadmcpfiles }}
{{- end }}
{{- if .Values.dns_resolver }}
    {{- $kubeadmcpfiles = include "resolv_conf" . | append $kubeadmcpfiles -}}
{{- end }}
{{- if (.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $kubeadmcpfiles = include "registry_mirrors" . | append $kubeadmcpfiles  -}}
{{- end }}
{{- if .Values.proxies.http_proxy }}
    {{-  $kubeadmcpfiles = include "containerd_proxy_conf" . | append $kubeadmcpfiles -}}
{{- end }}
{{- $additional_files := mergeOverwrite (deepCopy .Values.additional_files) (deepCopy .Values.control_plane.additional_files) }}
{{- if $additional_files }}
    {{- $kubeadmcpfiles = tuple . $additional_files | include "additional_files" | append $kubeadmcpfiles -}}
{{- end }}
{{- if $kubeadmcpfiles -}}
  {{- range $kubeadmcpfiles -}}
    {{ . | indent 2 }}
  {{- end }}
{{- else }}
    []
{{- end }}
postKubeadmCommands:
  - set -e
  - echo "Kubeadm bootstrap was successful" > /var/log/my-custom-file.log
{{- end }}
